# Actualizaciones

Puedes llevar a cabo las actualizaciones del sistema desde el panel de control, en 'Sistema' > 'Actualizar'.

![](img/updates/updates.png)


Si le das al botón "Actualizar" se llevarán a cabo todas las actualizaciones del sistema que estén disponibles en ese momento.

Este proceso llevará unos minutos durante los cuales no podrás usar el panel de control. Si hace mucho que no actualizas este proceso puede ser más largo.

Si hubiera una nueva Release de MaadiX también se actualizará mediante este proceso.

Se recomienda visitar el panel de control cada cierto tiempo, actualizar y revisar como están los detalles del sistema ('Sistema'> 'Detalles').


## Actualizaciones de MaadiX

Cuando hay una nueva versión de MaadiX (una nueva _release_) lo vas a ver indicado en el panel de control, en el apartado 'Actualizar'.

Las actualizaciones de MaadiX pueden implicar mejoras en el panel de control, actualizaciones de aplicaciones, corrección de bugs, implementación de nuevas aplicaciones, etc.

Se recomienda mantener el sistema actualizado con la última _release_ de MaadiX para tener una mejor experiencia y evitar problemas de seguridad.


## Actualizaciones automáticas

Los servidores de MaadiX tienen un sistema de actualizaciones automáticas (usando [_unattended-upgrades_](https://wiki.debian.org/UnattendedUpgrades)) que mantienen actualizado el sistema operativo y los paquetes instalados.

Estas actualizaciones automáticas **no actualizar a nuevas release de MaadiX**, para ello hay que hacerlo manualmente desde el panel de control.

A pesar de que haya este sistema de actualizaciones automáticas se recomienda ir al panel de control cada cierto tiempo y darle al botón "actualizar".


## Reinicio del sistema

Cuando se llevan a cabo ciertas actualizaciones es necesario que el servidor se reinicie. Esto ocurre, por ejemplo, cuando se actualiza el kernel del sistema operativo.

En estos casos saldrá un aviso en el panel de control que indicará que es necesario reiniciar el sistema para que se carguen las ultimas actualizaciones.

![](img/updates/AvisoReboot.png)

Para reiniciar el servidor solo hay que darle al botón "Reiniciar".

**Aviso**: durante el proceso de reinicio todos los servicios se detendrán (incluido el servidor web, el correo electrónico, etc). No debería tardar más de unos dos minutos.

## Actualizaciones de las aplicaciones en MaadiX

Las aplicaciones que instalas desde el panel de control se actualizarán con cada nueva _release_ de MaadiX.

Sin embargo, la actualización de sus plugins, complementos o apps internas se tendrá que hacer en cada aplicación manualmente, como es el caso de las aplicaciones de [Nextcloud](ncloud).

Las actualizaciones de Wordpress, Drupal (y otros gestores de contenido) que se instalen en el servidor también tendrán que hacerse manualmente.

**Recuerda que mantener todo el sistema actualizado es indispensable para evitar vulnerabilidades de seguridad**.
