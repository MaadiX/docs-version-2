# VPN

Una red privada virtual o VPN ([Virtual Private Network - Wikipedia](https://es.wikipedia.org/wiki/Red_privada_virtual)) es una forma de proteger todo tu tráfico a través de una conexión cifrada, segura y directa a tu servidor, garantizando la confidencialidad de tu navegación incluso en las circunstancias más adversas (redes públicas o poco fiables).

La VPN de MaadiX te permite conectarte a tu servidor y gestionarlo utilizando en todo momento una conexión cifrada y segura. Además, también puedes visitar cualquier dirección de Internet canalizando tu tráfico a través del servidor, lo que te permite acceder a contenidos que podrían estar bloqueados en el país en el que te encuentras físicamente.

Para empezar a usar la VPN de tu servidor de MaadiX debes llevar a cabo tres procesos:
* Instalar el servidor VPN (OpenVPN)
* Crear o editar una cuenta desde tu panel de control para darle acceso a la VPN
* Instalar un programa 'cliente' en el dispositivo que quieres conectar a través de la VPN.

A continuación, se explican paso a paso estas operaciones, sin las cuales no podrás disfrutar de las ventajas de una conexión mediante VPN.

## Instalación del servidor VPN

Desde el panel de control, ve al apartado '**Instalar Aplicaciones**', ahí podrás ver todas las aplicaciones disponibles para instalar, entre ellas el Servidor VPN (OpenVPN). Solo tienes que marcar la casilla _'Seleccionar'_ y darle al botón de **"Instalar"**.

Después de unos minutos en los que se hace la instalación ya tendrás tu servidor VPN instalado en Maadix.


## Crear o editar una cuenta

1. Estando en el panel de control, entra en la pestaña '**Usuarixs**' > '**Cuentas Ordinarias**'. Allí puedes editar una cuenta existente o bien crear una nueva.

2. Entre las opciones disponibles dentro del panel de edición, verás **'*Activar Cuenta VPN***'. Marca esta casilla para activar la cuenta VPN. Si no tienes instalado el servidor VPN esta opción no estará disponible.

3. Marca también la casilla '*Enviar instrucciones*' para enviar a la persona un **correo electrónico con los archivos de configuración** y las correspondientes instrucciones para la configuración del cliente VPN. Recuerda que las instrucciones incluyen todos los datos de configuración necesarios menos la contraseña, que por razones de seguridad debes proporcionarla por otro canal seguro.

![Screenshot](img/vpn/vpn_user.png)

**Nota**: en caso de que quieras volver a enviar este correo con los archivos de configuración puedes hacerlo de la siguiente manera: Editar la cuenta > marcar la casilla '*Enviar instrucciones*' > pulsar el botón 'Guardar'. Las instrucciones se enviarán al correo electrónico asociado a esta la cuenta.

## Instalar y configurar el cliente OpenVPN

Para establecer una conexión VPN con el servidor necesitas una aplicación cliente. OpenVPN será la solución de software libre y código abierto que necesitas usar para Maadix. A continuación, puedes encontrar un tutorial detallado para ordenadores con los sistemas operativos Windows, Linux, MacOS, así como para móviles Android.


### Windows

1\. Descarga e instala la aplicación OpenVPN desde [este enlace](https://openvpn.net/community-downloads/).

![Screenshot](img/vpn/windows-vpn/vpn_win1.png)

Elige el ejecutable (.exe) que necesites según tu versión de Windows. Puedes proceder a la instalación con la configuración por defecto.

Este es un ejemplo de una instalación en Windows 10:

![Screenshot](img/vpn/windows-vpn/vpn_win2.png)

Como ves, por defecto, OpenVPN se instalará en la ruta: `C:\Program Files\OpenVPN`

![Screenshot](img/vpn/windows-vpn/vpn_win3.png)

2\. Ahora vas a necesitar los archivos que se te enviaron por correo a la cuenta para la que se activó el servicio de VPN. Descomprime el archivo `.zip` que se te ha enviado y accede a la carpeta 'Windows'. Allí encontrarás los dos archivos que necesitas para esta configuración:

a) `vpn.ovpn`

b) `ca.crt`

Tendrás que copiarlos a en la ruta: `C:\Program Files\OpenVPN\config\`

![Screenshot](img/vpn/windows-vpn/vpn_win4.png)

Nota: si durante la instalación pusiste otra ruta diferente a la que venía por defecto entonces tendrás que ponerla en `\tu_ruta\config`.

3\. Abre la aplicación OpenVPN GUI. Es probable que se haya creado un acceso directo en el escritorio. También se te creará un icono en el área de notificaciones (o System Tray) como puedes ver en esta imagen:

![Screenshot](img/vpn/windows-vpn/vpn_win5.png)

4\. Selecciona 'Conectar' e introduce el nombre de la cuenta y contraseña enviados para efectuar la conexión.

![Screenshot](img/vpn/windows-vpn/vpn_win6.png)

![Screenshot](img/vpn/windows-vpn/vpn_win7.png)

Espera unos segundos hasta que se establezca la conexión. Para comprobar que la conexión se ha efectuado con éxito, visita cualquier web que te indique la dirección IP con la que estás navegando (por ejemplo [cualesmiip.com](https://cualesmiip.com/)), comprueba que esa IP cambia cuando activas o desactivas la conexión VPN.


### Linux

1\. Instala el cliente OpenVPN si no lo tienes instalado todavía (muchas distribuciones de Linux lo incluyen por defecto).

Por consola:

    sudo apt install network-manager-openvpn
    sudo apt install network-manager-openvpn-gnome

    sudo restart network-manager

Con el gestor de paquetes Synaptic:

Aplicaciones > Herramientas de Sistema > Gestor de paquetes Synaptic

Busca y selecciona _network-manager-openvpn_ y _network-manager-openvpn-gnome_ e instálalos.

![Screenshot](img/vpn/linux-vpn/01-install.png)

2\. Haz clic en 'Configuración de Red' desde el panel del _Network Manager_ (el nombre puede ser también 'Preferencias de Red', 'Conexiones de Red' u otro, dependiendo de tu distribución de Linux).

![Screenshot](img/vpn/linux-vpn/ubuntu-edit.conn.png)

3\. Busca el botón 'Añadir' o simplemente '+' para añadir la nueva configuración y elige la opción VPN/OpenVPN.

![Screenshot](img/vpn/linux-vpn/03-add-vpn.png)

4\. Escoge la opción 'Importar desde un archivo'.

![Screenshot](img/vpn/linux-vpn/03-import-profile.png)

5\. Ahora vas a necesitar los archivos que te han llegado por correo. Descomprime el archivo `.zip` y localiza los archivos `vpn.conf` y `ca.crt` dentro de la carpeta 'Linux'.

Selecciona el archivo `vpn.conf`
![Screenshot](img/vpn/linux-vpn/04-select-file.png)

6\. Introduce el nombre de la cuenta y contraseña para llevar a cabo la conexión. Elige el método de gestión de la contraseña que prefieras (es aconsejable la opción 'Preguntar siempre').

![Screenshot](img/vpn/linux-vpn/05-insert-data-vpn.png )

7\. Vuelve al Network Manager para activar la conexión recién creada.

![Screenshot](img/vpn/linux-vpn/07-connected-vpn.png)

Espera unos segundos hasta que se establezca la conexión. Para comprobar que la conexión se ha efectuado con éxito, visita la web [http://cualesmiip.com/](https://cualesmiip.com/) primero con la VPN activada y luego desactivada. El resultado que te ofrece como 'Tu IP real' debería ser distinto.

### MacOS

1\. Instala el cliente VPN para MAC Tunnelblick, puedes descargarlo [aquí](https://tunnelblick.net/downloads.html)  (elige la versión estable).

![](img/vpn/mac-vpn/tunnelblick.png)

2\. Una vez instalado Tunnelblick hay que añadir los archivos de configuración que te han llegado por correo. Pulsa en el botón **+** y selecciona la carpeta `mac.tblk`.

![](img/vpn/mac-vpn/conf_files.png)

3\. Pulsa el botón **"Connect"** y se te pedirá el nombre de la cuenta y la contraseña para iniciar la conexión.

![](img/vpn/mac-vpn/connect.png)

Para comprobar que la conexión se ha efectuado con éxito, visita la web [http://cualesmiip.com/](http://cualesmiip.com/) y verifica que la dirección IP que te indica es diferente a la de tu conexión habitual.

### Android

1\. Extrae y guarda en tu dispositivo los archivos de configuración de la VPN que te han llegado por correo electrónico. Solo necesitas los dos archivos que están dentro de la carpeta 'Android'.

2\. Descarga en [Google Play](https://play.google.com/store/apps/details?id=net.openvpn.openvpn&hl=es) o [F-Droid](https://f-droid.org/app/de.blinkt.openvpn) la aplicación **OpenVpn Connect**.

3\. Abre la aplicación. En el menú, elige la opción 'Import Profile' > File y seleccionar el fichero `android-client.ovpn` en la carpeta donde lo hayas descargado. Finalmente pulsa 'Import'.

No hace falta seleccionar el fichero `ca.crt` pero tiene que permanecer en la misma carpeta que `android-client.ovpn` para que se configure correctamente el 'profile'.

<img src="img/vpn/android-vpn/vpn_android1.jpg" width="250">

4\. Introduce el nombre de la cuenta y pulsa 'Add'.

<img src="img/vpn/android-vpn/vpn_android3.jpg" width="250">

5\. Entonces ahora para establecer la conexión tienes que pulsar el boton (tipo interruptor, en gris).

<img src="img/vpn/android-vpn/vpn_android4.jpg" width="250">

Te pedirá la contraseña:

<img src="img/vpn/android-vpn/vpn_android2.jpg" width="250">

Espera unos segundos hasta que se establezca la conexión. Deverías ver algo así con el "Connected" en verde:

<img src="img/vpn/android-vpn/vpn_android5.jpg" width="250">


Para comprobar que la conexión se ha efectuado con éxito, visita la web [http://cualesmiip.com/](http://cualesmiip.com/) primero con la VPN activada y luego desactivada. El resultado que te ofrece como 'Tu IP real' debería ser distinto.
