# Crea tu web o aplicación

El resumen de los pasos a seguir para alojar una página web en un servidor de MaadiX son los siguientes:

1. Crear una cuenta ordinaria con acceso SFTP o SSH para que sea la webmaster de la web.
2. Apuntar el dominio o subdomino de la página web a la IP del servidor (operación externa a MaadiX).
3. Añadir el dominio o subdomino al panel de control y asignarle como webmaster la cuenta creada anteriormente.
4. Esperar hasta que aparece "Servidor web - Activado" en verde (puede tardar unos segundos).
5. Conectarse al servidor con la cuenta webmaster para subir los ficheros de la web al directorio `/var/www/html/ejemplo.com`

Más abajo puedes encontrar una explicación más detallada de cada paso.

Recuerda que **podrás alojar tantas webs como desees**, no hay límite de dominios, solo tienes que contar con suficiente espacio y recursos en el servidor.


## La cuenta webmaster de tu web

Lo primero que necesitas para gestionar una web en un servidor de MaadiX es una **cuenta ordinaria con acceso por SSH o SFTP que haga de webmaster**.

Recuerda que la cuenta **Superusuarix no podrá ser asignada como webmaster**.

Esta funcionalidad está pensada para que la persona que hace el desarrollo y actualizaciones de una web tenga su propia cuenta y permisos limitados, solo los necesarios para subir los archivos que conforman la web. No podrá hacer otras modificaciones en el servidor.

Esto es útil para organizaciones en las que la persona que administra el servidor es diferente a la que hace el desarrollo de la web.

Para crear una cuenta ordinaria tedrás que ir a el panel de control, a la sección 'Usuarixs'> 'Cuentas ordinarias' y darle al botón "Crear nueva cuenta".

**Para que una cuenta ordinaria pueda ser asignada como webmaster tiene que tener permisos de acceso por SFTP o SSH**. Puedes elegirlo en el momento de creación de la cuenta en el apartado "Acceso SSH o SFTP al servidor".

Una misma cuenta podrá ser asignada como webmaster de varios dominios.

En el caso de que tu web o aplicación web haga uso de bases de datos (como es el caso de Wordpress), probablemente sea útil que le actives a esta cuenta el acceso a phpMyAdmin (puedes ver más detalles [aquí](mysql.md)).


## El dominio de tu web

### Apuntar el dominio al servidor

Para poder crear un sitio web accesible desde cualquier navegador necesitarás un dominio o subdominio que tendrás que registrar con algún proveedor (por ejemplo, [gandi.net](https://www.gandi.net/) o [Njal.la](https://njal.la/), hay muchos más en Internet).

Una vez ya tienen tu dominio, puedes ir a su editor de zonas DNS y añadir un registro A que apunte a la IP de tu servidor (la IP de tu servidor la puedes ver en el Panel de Control > Dominios > Instrucciones).

Recuerda que **esta es una operación externa a MaadiX** y que cada proveedor y editor de zona DNS es diferente. **MaadiX no es un proveedor de dominios ni ofrece un editor de zonas DNS.**

Normalmente el proveedor con el que registras tu dominio te ofrece un editor de zonas DNS al que puedes añadir registros.

Ejemplos:

| Tipo | Nombre | Valor |
|------|--------|-------|
| A    | @      | IP.DE.TU.SERVIDOR  |
| A    | subdominio   | IP.DE.TU.SERVIDOR  |




### Añadir el dominio en MaadiX

Para añadir tu dominio o subdominio tendrás que ir al Panel del Control, al apartado 'Dominios'> 'Añadir un dominio nuevo'.

En este momento tienes que asignar como webmaster la cuenta ordinaria creada anteriormente, selecciónala del desplegable. Puedes editarla posteriormente si lo necesitas.

![](img/CrearWeb/webmaster.png)

En [esta documentación](dominios.md) puedes encontrar los detalles de como se gestionan los dominios en MaadiX.

Una vez añades el dominio, el servidor comprobará si este dominio está apuntando a su IP, si es correcto, se activará el servidor web y verás en el panel "Servidor web: Activado" en verde.

![](img/CrearWeb/ServidorWeb.png)

Puede tardar unos segundos hasta que se active.

Tambien se puede añadir en primer lugar el dominio al panel de control, aunque el servidor web no se active, y posteriormente apuntar el dominio al servidor. El orden de estos pasos es indiferente y hay personas que lo prefieran hacerlo así.

Recuerda que cuando se añade un dominio en el Panel de Control, en el servidor se crea el directorio `/var/www/html/ejemplo.com`, aunque aún el servidor web esté desactivado. Sin embargo, puede ser útil para planificar migraciones e ir subiendo los archivos que conforman la web antes de cambiar los DNS.


**Nota**: en el caso de que no tengas un dominio propio puedes usar *subdominio.maadix.org*, donde 'subdominio' coincide con el nombre que elegiste al adquirir tu servidor MaadiX. En este caso, tienes que subir los archivos de la web a `/var/www/html/subdominio.maadix.org` con la cuenta Superusuarix.


## Subir contenidos

Una vez hayas añadido el dominio, tendrás que subir los archivos de tu web o aplicación a al directorio `/var/www/html/ejemplo.com`.   

Puedes acceder a esta ubicación con la cuenta asignada como webmaster y subir los archivos utilizando un cliente SFTP. En caso de que no tengas ningún cliente SFTP instalado y no supieras cual escoger, [Filezilla](https://filezilla-project.org/) es uno de los más usados y sencillos.

Recuerda que, por una cuestión de seguridad, en en los servidores MaadiX **no están permitidas las conexiones por FTP**, solo por SFTP.  

### Con una cuenta SFTP

Este será el caso en el que hayas asignado como webmaster una cuenta que solo tiene acceso por SFTP. Las cuentas SFTP solo tendrán acceso por SFTP y no por terminal (por SSH).

Esta cuenta será la propietaria de la carpeta `/var/www/html/midominio.com`, de manera que sólo conectando con esta cuenta podrás modificar los archivos que se encuentran en ella.

Las credenciales para la conexión haciendo uso de Filezilla (u otro cliente SFTP), serán las siguientes:

* **Servidor**: sftp://subdomino.maadix.org
* **Protocolo**: SFTP (recuerda especificarlo).
* **Nombre de la cuenta**: la cuenta Webmaster.
* **Contraseña**: la contraseña de la cuenta Webmaster.
* **Puerto**: por defecto será el puerto 22 (ver más en el apartado [seguridad](security.md)).


![Screenshot](img/CrearWeb/SftpWeb.png)


Cuando se establezca la conexión desde una cuenta SFTP verás una carpeta con su nombre. En ella, encontrarás una carpeta con el nombre del dominio o subdominio (que es un enlace simbólico a `/var/www/html/ejemplo.com`). Podrán haber varias carpetas si esta cuenta es Webmaster de otros dominios.

También puede tener ahí otros archivos propios que haya subido o creado anteriormente. Las cuentas Webmaster sólo tienen acceso a esta zona y no a todos los archivos del sistema (como sí tiene la cuenta Superusuarix).

En la carpeta con el nombre del dominio `/Webmaster/ejemplo.com` es donde se tendrán que subir los ficheros que conforman la web o aplicación web. Se hará fácilmente arrastrando los archivos desde el equipo local.

**Nota**: si haces demasiados intentos de conexión fallidos (más de 6) con un cliente SFTP el servidor bloqueará la IP de tu conexión (de tu oficina o vivienda). Encuentra más información [aquí](fail2ban.md).

### Con una cuenta SSH

Este será el caso en el que hayas asignado como webmaster una cuenta que tiene acceso por SFTP y por SSH. Esta cuenta sí podrá acceder al servidor tanto por terminal (por SSH) como como utilizando un cliente SFTP.

La forma de acceder por SFTP es parecida a la descrita en el apartado anterior, pero en este caso la ubicación en la que aterrizas cuando se establece la conexión es diferente. Estas cuentas disponen de un directorio propio en `/home/USERNAME` y al contrario de las cuentas SFTP no están enjauladas en él. Pueden acceder a otros directorios y efectuar operaciones de lectura/escritura sobre aquellos archivos de los que sean propietarias.    

Tendrás que navegar a través de las carpetas hasta llegar a `/var/www/html/ejemplo.com/`, que es donde tendrás que subir o crear los archivos de tu nueva web o aplicación. Otra opción más rápida es escribir la ruta `/var/www/html/ejemplo.com/` en el campo "Sitio remoto".

Una vez ahí, puedes seguir los mismos pasos descritos para las cuentas SFTP.

![Screenshot](img/CrearWeb/SshWeb.png)


## Certificados con Let's Encrypt

Cuando añades un dominio en el Panel de Control de la forma que te describimos anteriormente, automáticamente se genera y se configura un certificado con [Let's Encrypt](https://letsencrypt.org/) para este dominio.

No tendrás que hacer nada más para configurar los certificados. Las conexiones a tu web o aplicación web se harán por https.
