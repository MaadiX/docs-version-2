
# Nextcloud

Nextcloud es un software (libre y de código abierto) que te permite crear una nube segura donde almacenar tus archivos. Es una alternativa a servicios como Dropbox o Google Drive.

Además, también se le pueden añadir aplicaciones de calendario, gestión de tareas, gestión de contactos, videollamadas, etc.


## Instalación

Desde el panel de control, ve al apartado '**Instalar Aplicaciones**', ahí podrás ver todas las aplicaciones disponibles para instalar, entre ellas Nextcloud. Solo tienes que marcar la casilla _'Seleccionar'_ y darle al botón de **"Instalar"**.

Después de unos minutos en los que se hace la instalación ya tendrás tu nube segura con Nextcloud instalada en Maadix.

Después de este proceso te llegará un email (a la dirección que tengas configurada para la cuenta admin del panel de control) con los datos de acceso a Nextcloud. Cambia la contraseña lo antes posible.


## Acceso

Para acceder a Nextcloud tendrás que ir a `<la-url-de-tu-servidor>/nextcloud` , es decir a `myserver.maadix.org/nextcloud` o `myserver.example.com/nextcloud` (si has configurado tu propio domino para el servidor).

También puedes hacer ir al cpanel de MaadiX y en el menú ir a Mis Aplicaciones > Nextcloud y te llevará a la url de acceso.

![](img/nextcloud/nc-acceso.png)

Allí puedes hacer login con las credenciales que se te han llegado por correo.

![](img/nextcloud/nc-acceso2.png)

## Customización de la url

Si quieres que tu instalación de Nextcloud sea accesible en un subdominio tipo `nextcloud.example.com` tendrás que hacer lo siguiente:

1. Crear un registro DNS de tipo A, que apunte a la IP del servidor:

 `nextcloud.example.com A IP.DE.TU.SERVIDOR`

2. Añadir el subdominio `nextcloud.example.com , desde tu panel de control, en la pestaña '**Dominios**' -> '**Añadir un dominio nuevo**'. Recuerda como hacerlo en la documentación sobre [dominios](/dominios). No es necesario que le asignes ningún webmaster, ni que actives el servidor de correo ni el DKIM para este dominio.

3. Añadir el subdominio a `trusted_domains`:

Acceder por ssh al servidor con la cuenta Superuser y editar el fichero `config.php` que estará en la ruta `/var/www/nextcloud/nextcloud/config/`(tienes que ser root para poder editar este archivo). Tendrás que editarlo desde la consola con algún editor tipo nano/vim y añadir el subdominio al array de `trusted_domains`, te quedará algo así:

```
  'trusted_domains' =>
    array (
      0 => 'myserver.example.com',
      1 => 'IP.DE.TU.SERVIDOR',
      2 => 'nextcloud.example.com',
    ),
```

4. Crear un fichero `index.php` en la ruta `/var/www/html/nextcloud.example.com/`

y añadirle el siguiente contenido:



```
<?php
   header("Location: https://" . $_SERVER['HTTP_HOST'] ."/nextcloud");
   die();
```

A partir de ahora podrás acceder a nextcloud a través del subdominio https://nextcloud.example.com


# Actualizaciones

Las actualizaciones de Nextcloud se harán de forma automática cuando hagas la actualización a una nueva release de MaadiX (ver documentación sobre [actualizaciones](updates))

Sin embargo, las actualizaciones de las aplicaciones que instales en Nextcloud las tendrás que hacer desde Nextcloud: 'Aplicaciones' > 'Aplicaciones Activas' y allí darle al botón "Actualizar" en las aplicaciones que hayan sacado alguna actualización.   
